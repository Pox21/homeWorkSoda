public class Soda {
    String type;
    Soda() {}
    Soda(String type) {
        this.type = type;
    }
    public String getMyDrinkString() {
        if (this.type == null || this.type.isEmpty()) {
            return "Обычная газировка";
        }
        return String.format("Газировка(%s)", this.type);
    }
}
