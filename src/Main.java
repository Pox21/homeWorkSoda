public class Main {
//    Создайте класс Soda (для определения типа газированной воды).
//
//    У класса должен быть конструктор, принимающий 1 аргумент при инициализации - строку добавка (например, "малина"), отвечающий за добавку к выбираемому лимонаду.
//
//    В этом классе реализуйте метод public String GetMyDrinkString(), возвращающий строку Газировка и {добавка} в случае наличия добавки.
//
//    Если добавки нет, нужно вернуть строку "Обычная газировка".
    public static void main(String[] args) {
        Soda soda = new Soda();
        Soda soda2 = new Soda("");
        Soda sodaApple = new Soda("яблочный");

        System.out.println(soda.getMyDrinkString());
        System.out.println(soda2.getMyDrinkString());
        System.out.println(sodaApple.getMyDrinkString());
    }
}